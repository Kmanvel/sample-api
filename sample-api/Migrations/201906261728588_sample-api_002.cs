namespace sample_api.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class sampleapi_002 : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.Enrollments", "StudentId");
            CreateIndex("dbo.Enrollments", "SubjectId");
            AddForeignKey("dbo.Enrollments", "StudentId", "dbo.Students", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Enrollments", "SubjectId", "dbo.Subjects", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Enrollments", "SubjectId", "dbo.Subjects");
            DropForeignKey("dbo.Enrollments", "StudentId", "dbo.Students");
            DropIndex("dbo.Enrollments", new[] { "SubjectId" });
            DropIndex("dbo.Enrollments", new[] { "StudentId" });
        }
    }
}
