﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sample_api.Models
{
    public class Subject
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Credits { get; set; }
        public int Term { get; set; }

        public virtual ICollection<Enrollment> Enrollments { get; set; }
    }
}