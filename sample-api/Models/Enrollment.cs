﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sample_api.Models
{
    public class Enrollment
    {
        public int Id { get; set; }
        public int StudentId { get; set; }
        public int SubjectId { get; set; }
        public int Point { get; set; }
    }
}