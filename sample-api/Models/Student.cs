﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sample_api.Models
{
    public class Student
    {
        public int Id { get; set; }
        public string PersonalId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
        public DateTime RegistrationDate { get; set; }
        public bool IsActive { get; set; }

        public virtual ICollection<Enrollment> Enrollments { get; set; }
    }
}