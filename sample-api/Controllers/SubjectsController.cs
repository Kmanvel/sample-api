﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using sample_api.Models;
using sample_api.Repositories;
using WebGrease.Css.Ast;

namespace sample_api.Controllers
{
    public class SubjectsController : ApiController
    {
        [HttpGet]
        [Route("api/subjects")]
        public IEnumerable<Subject> Get()
        {
            using (var db = new UniversityDbContext())
            {
                return db.Subjects.ToList();
            }
        }

        [HttpGet]
        [Route("api/subjects/{id}")]
        public Subject Get(int id)
        {
            using (var db = new UniversityDbContext())
            {
                return db.Subjects.Find(id);
            }
        }

        [HttpPost]
        [Route("api/subjects")]
        public void Post([FromBody]string name, string description, int credits, int term)
        {
            using (var db = new UniversityDbContext())
            {
                db.Subjects.Add
                (
                    new Subject
                    {
                        Name = name,
                        Description = description,
                        Credits = credits,
                        Term = term
                    }
                );

                db.SaveChanges();
            }
        }

        [HttpPut]
        [Route("api/subjects/{id}")]
        public void Put(int id, [FromBody]string name, string description, int credits, int term)
        {
            using (var db = new UniversityDbContext())
            {
                var subject = db.Subjects.Find(id);
                if (subject == null) return;
                subject.Name = name;
                subject.Description= description;
                subject.Credits = credits;
                subject.Term = term;
                db.SaveChanges();
            }
        }

        [HttpDelete]
        [Route("api/subjects/{id}")]
        public void Delete(int id)
        {
            using (var db = new UniversityDbContext())
            {
                var subject = db.Subjects.Find(id);
                if (subject == null) return;
                db.Subjects.Remove(subject);
                db.SaveChanges();
            }
        }
    }
}
