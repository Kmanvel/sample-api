﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using sample_api.Models;
using sample_api.Repositories;

namespace sample_api.Controllers
{
    public class EnrollmentsController : ApiController
    {
        [HttpGet]
        public IEnumerable<Enrollment> Get()
        {
            using (var db = new UniversityDbContext())
            {
                return db.Enrollments.ToList();
            }
        }

        [HttpGet]
        public Enrollment Get(int id)
        {
            using (var db = new UniversityDbContext())
            {
                return db.Enrollments.Find(id);
            }
        }

        [HttpPost]
        [Route("api/enrollments")]
        public void Post([FromBody] int studentId, int subjectId, int point)
        {
            using (var db = new UniversityDbContext())
            {
                db.Enrollments.Add
                (
                    new Enrollment
                    {
                        StudentId = studentId,
                        SubjectId = subjectId,
                        Point = point
                    }
                );

                db.SaveChanges();
            }
        }

        [HttpPut]
        [Route("api/enrollments/{id}")]
        public void Put(int id, [FromBody] int subjectId, int point)
        {
            using (var db = new UniversityDbContext())
            {
                var enrollment = db.Enrollments.Find(id);
                if (enrollment == null) return;
                enrollment.SubjectId = subjectId;
                enrollment.Point = point;
                db.SaveChanges();
            }
        }

        [HttpDelete]
        [Route("api/enrollments/{id}")]
        public void Delete(int id)
        {
            using (var db = new UniversityDbContext())
            {
                var enrollment = db.Enrollments.Find(id);
                if (enrollment == null) return;
                db.Enrollments.Remove(enrollment);
                db.SaveChanges();
            }
        }
    }
}
