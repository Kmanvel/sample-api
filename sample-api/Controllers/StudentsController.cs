﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using sample_api.Models;
using sample_api.Repositories;

namespace sample_api.Controllers
{
    public class StudentsController : ApiController
    {

        [HttpGet]
        [Route("api/students")]
        public IList<Student> Get()
        {
            using (var db = new UniversityDbContext())
            {
                return db.Students.ToList();
            }
        }

        [HttpGet]
        [Route("api/students/{id}")]
        public Student Get(int id)
        {
            using (var db = new UniversityDbContext())
            {
                return db.Students.Find(id);
            }
        }

        [HttpPost]
        [Route("api/students")]
        public void Post([FromBody]string personalId, string firstName, string lastName, DateTime birthDate, bool isActive)
        {
            using (var db = new UniversityDbContext())
            {
                db.Students.Add
                (
                    new Student
                    {
                        PersonalId = personalId,
                        FirstName = firstName,
                        LastName = lastName,
                        RegistrationDate = DateTime.Now,
                        BirthDate = birthDate,
                        IsActive = isActive
                    }
                );

                db.SaveChanges();
            }
        }

        [HttpPut]
        [Route("api/students/{id}")]
        public void Put(int id, [FromBody]string personalId, string firstName, string lastName, DateTime birthDate, bool isActive)
        {
            using (var db = new UniversityDbContext())
            {
                var student = db.Students.Find(id);
                if (student == null) return;
                student.PersonalId = personalId;
                student.FirstName = firstName;
                student.LastName = lastName;
                student.BirthDate = birthDate;
                student.IsActive = isActive;
                db.SaveChanges();
            }
        }

        [HttpDelete]
        [Route("api/students/{id}")]
        public void Delete(int id)
        {
            using (var db = new UniversityDbContext())
            {
                var student = db.Students.Find(id);
                if (student == null) return;
                db.Students.Remove(student);
                db.SaveChanges();
            }
        }
    }
}
